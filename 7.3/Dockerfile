# install noverify
FROM golang:alpine as vkcom-noverify
RUN set -xe && \
    apk add --update --no-cache \
    git \
    musl-dev && \
    go get -u github.com/VKCOM/noverify && \
    cd $GOPATH/src/github.com/VKCOM/noverify && \
    go build && \
    git clone https://github.com/JetBrains/phpstorm-stubs.git --depth 1 /usr/src/phpstorm-stubs

# phpqa dockerfile for php:7.3-alpine
FROM php:7.3-alpine

LABEL maintainer="Jakub Zalas <jakub@zalas.pl>"

ENV BUILD_DEPS="autoconf file g++ gcc libc-dev pkgconf re2c"
ENV LIB_DEPS="zlib-dev libzip-dev"
ENV TOOL_DEPS="git graphviz make unzip"
ENV TOOLBOX_EXCLUDED_TAGS="exclude-php:7.3"
ENV TOOLBOX_TARGET_DIR="/tools"
ENV PATH="$PATH:$TOOLBOX_TARGET_DIR:$TOOLBOX_TARGET_DIR/.composer/vendor/bin:$TOOLBOX_TARGET_DIR/QualityAnalyzer/bin:$TOOLBOX_TARGET_DIR/DesignPatternDetector/bin:$TOOLBOX_TARGET_DIR/EasyCodingStandard/bin"
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME=$TOOLBOX_TARGET_DIR/.composer

COPY --from=composer:1.9 /usr/bin/composer /usr/bin/composer

RUN apk add --no-cache --virtual .tool-deps $TOOL_DEPS $LIB_DEPS \
 && apk add --no-cache --virtual .build-deps $BUILD_DEPS  coreutils\
 && git clone https://github.com/nikic/php-ast.git && cd php-ast && phpize && ./configure && make && make install && cd .. && rm -rf php-ast && docker-php-ext-enable ast \
 && docker-php-ext-install zip pcntl \
 && echo "date.timezone=Europe/London" >> $PHP_INI_DIR/php.ini \
 && echo "memory_limit=-1" >> $PHP_INI_DIR/php.ini \
 && echo "phar.readonly=0" >> $PHP_INI_DIR/php.ini \
 && TOOLBOX_VERSION="$(git ls-remote --tags https://github.com/jakzal/toolbox.git | sort -t '/' -k 3  -V|cut -d "/" -f 3|grep -o -E '[0-9]{0,}\.[0-9]{0,}\.[0-9]{0,}'|tail -n 1)" \
 && echo "TOOLBOX_VERSION=${TOOLBOX_VERSION}" \
 && mkdir -p $TOOLBOX_TARGET_DIR && curl -Ls https://github.com/jakzal/toolbox/releases/download/v$TOOLBOX_VERSION/toolbox.phar -o $TOOLBOX_TARGET_DIR/toolbox && chmod +x $TOOLBOX_TARGET_DIR/toolbox \
 && php $TOOLBOX_TARGET_DIR/toolbox install \
 && rm -rf $COMPOSER_HOME/cache \
 && apk del .build-deps

CMD php $TOOLBOX_TARGET_DIR/toolbox list-tools

# Add tools
COPY --from=vkcom-noverify /go/src/github.com/VKCOM/noverify/noverify /usr/local/bin/
COPY --from=vkcom-noverify /usr/src/phpstorm-stubs /usr/src/phpstorm-stubs

RUN wget -O twig-lint https://asm89.github.io/d/twig-lint.phar \
    && chmod +x twig-lint \
    && mv twig-lint $TOOLBOX_TARGET_DIR/.composer/vendor/bin

RUN docker-php-ext-install pdo pdo_mysql calendar bcmath
RUN apk add --no-cache libxml2-dev \
    imagemagick-dev \
    ghostscript \
    imagemagick \
    freetype-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    icu \
 &&  apk add --no-cache --virtual .build-deps  \
            $PHPIZE_DEPS \
            zlib-dev  \
            icu-dev  \
 && docker-php-ext-install soap \
 && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
 && docker-php-ext-install gd \
 && pecl install imagick \
 && docker-php-ext-enable imagick \
 && docker-php-ext-install soap \
 && docker-php-ext-configure intl \
 && docker-php-ext-install intl \
 && docker-php-ext-enable intl \
 && apk del .build-deps icu-dev $PHPIZE_DEPS zlib-dev

 RUN composer global require friendsoftwig/twigcs
